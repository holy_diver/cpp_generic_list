# CPP_Generic_List

A simple implementation of C# generic list. Done for learning purposes only.

## Why?

Because why not?

## Which methods are implemented?

For now, Add, Remove, RemoveAt and Count methods are implemented along with [] operator overload but I will be updating the implementation in my free time.

**12/03/2021 21:08 Update:**

New methods are implemented but not yet tested.

- Clear()
- Contains(T)
- IndexOf(T)
- LastIndexOf(T)
- Reverse()
- Insert(int, T)

## Tests

| Method Signature | Test Status |
| - | - |
| Add(T) | PASS |
| Remove(T) | PASS |
| RemoveAt(int) | PASS |
| Count() | PASS |
| operator\[](int) | PASS |
| Clear() | NOT TESTED |
| Contains(T) | NOT TESTED |
| IndexOf(T) | NOT TESTED |
| LastIndexOf(T) | NOT TESTED |
| Reverse() | NOT TESTED |
| Insert(int, T) | PASS |

## Usage

### Creating a new instance:

```c++
List<int> *exampleList = new List<int>();
```

or

```c++
List<int> *list = (List<int> *)malloc(sizeof(List<int>));
List<int> listInStack;
*list = listInStack;
```

### Adding new element:

```c++
(*exampleList).Add(3);
```

or simply

```c++
exampleList->Add(3);
```

### Removing element

```c++
exampleList->RemoveAt(1);
```

or

```c++
exampleList->Remove(2132); //returns true if item exists and removed with success, else false.
```

### Getting and setting element

#### Get

```c++
int a = (*exampleList)[0];
```

or

```c++
int a = exampleList->operator[](0);
```

#### Set

```c++
(*exampleList)[0] = 12;
```

or

```c++
exampleList->operator[](0) = 12;
```
