#include <cstdlib>
#include "./List.h"

template <typename T>
void List<T>::Add(T item)
{
    if (arraySize == 0)
    {
        arrayPointer = (T *)malloc(sizeof(T));
        *arrayPointer = item;
        arraySize = 1;
    }
    else
    {
        arrayPointer = (T *)realloc(arrayPointer, ++arraySize * sizeof(T));
        arrayPointer[arraySize - 1] = item;
    }
}

template <typename T>
bool List<T>::Remove(T item)
{
    for (int i = 0; i < arraySize; i++)
    {
        if (arrayPointer[i] == item)
        {
            for (int j = i; j < arraySize - 1; j++)
            {
                arrayPointer[j] = arrayPointer[j + 1];
            }
            arrayPointer = (T *)realloc(arrayPointer, --arraySize * sizeof(T));
            return true;
        }
    }
    return false;
}

template <typename T>
void List<T>::RemoveAt(int index)
{
    for (int j = index; j < arraySize - 1; j++)
    {
        arrayPointer[j] = arrayPointer[j + 1];
    }
    arrayPointer = (T *)realloc(arrayPointer, --arraySize * sizeof(T));
}

template <typename T>
void List<T>::Clear()
{
    arraySize = 0;
    free(arrayPointer);
    arrayPointer = nullptr;
}

template <typename T>
int List<T>::IndexOf(T item)
{
    for (int i = 0; i < arraySize; i++)
    {
        if (arrayPointer[i] == item)
        {
            return i;
        }
    }
    return -1;
}

template <typename T>
int List<T>::LastIndexOf(T item)
{
    int index = -1;
    for (int i = 0; i < arraySize; i++)
    {
        if (arrayPointer[i] == item)
        {
            index = i;
        }
    }
    return index;
}

template <typename T>
void List<T>::Reverse()
{
    T temp;
    for (int i = 0; i < arraySize / 2; i++)
    {
        temp = arrayPointer[i];
        arrayPointer[i] = arrayPointer[arraySize - i - 1];
        arrayPointer[arraySize - i - 1] = temp;
    }
}

template <typename T>
void List<T>::Insert(int index, T item)
{
    arrayPointer = (T *)realloc(arrayPointer, ++arraySize * sizeof(T));
    for (int i = arraySize - 1; i > index; i--)
    {
        arrayPointer[i] = arrayPointer[i - 1];
    }
    arrayPointer[index] = item;
}

template <typename T>
int List<T>::Count()
{
    return arraySize;
}

template <typename T>
bool List<T>::Contains(T item)
{
    for (int i = 0; i < arraySize; i++)
    {
        if (arrayPointer[i] == item)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
T &List<T>::operator[](int i)
{
    return arrayPointer[i];
}

template <typename T>
List<T>::List()
{
    arrayPointer = nullptr;
    arraySize = 0;
}